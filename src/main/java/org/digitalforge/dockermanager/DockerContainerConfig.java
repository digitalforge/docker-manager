package org.digitalforge.dockermanager;

import java.util.List;

public class DockerContainerConfig {

    private String name;
    private String imageName;
    private List<String> environment;
    private List<String> portBindings;

    public DockerContainerConfig(String name, String imageName) {
        if(name == null || name.isBlank()) {
            throw new IllegalArgumentException("'name' must not be blank!");
        }
        if(imageName == null || imageName.isBlank()) {
            throw new IllegalArgumentException("'imageName' must not be blank!");
        }
        this.name = name;
        this.imageName = imageName;
    }

    public String getName() {
        return name;
    }

    public String getImageName() {
        return imageName;
    }

    public DockerContainerConfig setName(String name) {
        if(name == null || name.isBlank()) {
            throw new IllegalArgumentException("'name' must not be blank!");
        }
        this.name = name;
        return this;
    }

    public List<String> getEnvironment() {
        return environment;
    }

    public DockerContainerConfig setEnvironment(List<String> environment) {
        this.environment = environment;
        return this;
    }

    public List<String> getPortBindings() {
        return portBindings;
    }

    public DockerContainerConfig setPortBindings(List<String> portBindings) {
        this.portBindings = portBindings;
        return this;
    }

}
